$(document).ready(function(){

  //MENU TOGGLE
  $('.menu-icon').click(function(){
    $('.menu').toggle('fast');
  });

  //SMOOTH SCROLL

  $('.right-half a').click(function(){
      $('html, body').animate({
          scrollTop: $( $(this).attr('href')).offset().top - 80
      }, 500);
      return false;
  });

  //ANIMATIONS

  let photo = $('.img').offset();
  let photo2 = $('.img-2').offset();
  let photo3 = $('.img-3').offset();

  $(window).scroll(function(){
    let scrollT = $(window).scrollTop();
    if(photo.top < (scrollT + 600) && $(window).width() > 900) {
      $('.img').css({
        "opacity":"1",
        "transform":"translateX(0px)"
      })
      $('.lms h3').css({
        "opacity":"1"
      })
      $('.lms p').css ({
        "opacity":"1"
      })

    } else if($(window).width() > 900) {
      $('.img').css({
        "opacity":"0",
        "transform":"translateX(-300px)"
      })
      $('.lms h3').css({
        "opacity":"0"
      })
      $('.lms p').css ({
        "opacity":"0"
      })
    };
  });

  $(window).scroll(function(){
    let scrollT = $(window).scrollTop();

    if(photo2.top < scrollT + 600 && $(window).width() > 800) {
      $('.img-2').css({
        "opacity":"1"
      })
      $('.website-blog h3').css({
        "opacity":"1"
      })
      $('.website-blog p').css ({
        "opacity":"1"
      })
      $('.website-blog ul li').css ({
        "opacity":"1"
      })

    } else if($(window).width() > 900) {
      $('.img-2').css({
        "opacity":"0"
      })
      $('.website-blog h3').css({
        "opacity":"0"
      })
      $('.website-blog p').css ({
        "opacity":"0"
      })
      $('.website-blog ul li').css ({
        "opacity":"0"
      })
    };
  });

  $(window).scroll(function(){
    let scrollT = $(window).scrollTop();

  if(photo3.top < scrollT + 600 && $(window).width() > 800) {

    $('.img-3').css({
      "opacity":"1",
      "transform":"translateX(0)"
    })
    $('.email h3').css({
      "opacity":"1"
    })
    $('.email p').css ({
      "opacity":"1"
    })
    $('.email ul li').css ({
      "opacity":"1"
    })


  } else if($(window).width() > 900) {
    $('.img-3').css({
      "opacity":"0",
      "transform":"translateX(-300px)"
    })
    $('.email h3').css({
      "opacity":"0"
    })
    $('.email p').css ({
      "opacity":"0"
    })
    $('.email ul li').css ({
      "opacity":"0"
    })
  };
});

});
