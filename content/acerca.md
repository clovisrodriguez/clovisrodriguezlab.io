---
title: "Acerca"
date: 2018-02-05T15:15:13-05:00
draft: false
image: images/banner-contacto.jpg
---

## Sobre mí

Desde muy joven siempre he tenido un interés y pasión grande por la tecnología, como nuevos inventos y en especial el mundo del software ha cambiado nuestras vidas. Debido a esto la vida me ha dado la oportunidad de estudiar, trabajar y aprender de diferentes disciplinas como el marketing, branding, diseño gráfico el diseño, la creación de videojuegos, animación, diseño y desarrollo web, lenguajes de programación y emprendimiento.

Gracias a estas experiencias he podido llegar a comprender cuales son los elementos claves para la creación de un producto digital que sirva a las necesidades de las personas y las organizaciones.

Junto a la determinación de impactar mi entorno, he creado este proyecto para ayudar a transformar la educación en mi ciudad y país ayudando a todo educador, bien sean una institución o un creador de contenido independiente como alguien que da clases de música a llegar a más personas y mejorar la calidad de la enseñanza y el aprendizaje.
